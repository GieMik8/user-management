const female = require('./assets/female.png');
const male = require('./assets/male.png');

const images = {
  'femaleUrl': female,
  'maleUrl': male
}

export default images;
