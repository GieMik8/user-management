import Vue from 'vue'
import App from './App.vue'
import VueResource from 'vue-resource';
import VueRouter from 'vue-router';
import { routes, titles } from './routes';
import VeeValidate from 'vee-validate';

Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(VeeValidate);

const router = new VueRouter({
  routes,
  mode: 'history',
});

router.afterEach((toRoute, fromRoute) => {
    window.document.title = 'G.M. - '+titles[toRoute.name];
    window.scrollTo(0, 0);
})

Vue.http.options.root = 'http://localhost:4444';

export const alertEvents = new Vue();

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
