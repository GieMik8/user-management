import Users from './users/Users.vue';
import UsersList from './users/UsersList.vue';
import UserViewPage from './users/UserViewPage.vue';
import UserEdit from './users/UserEdit.vue';
import UserCreate from './users/UserCreate.vue';
import ErrorPage from './404.vue';

export const routes = [
  { path: '', redirect: { name: 'users'} },
  { path: '/users', component: Users, children: [
      { path: '', component: UsersList, name: 'users' },
      { path: 'create', component: UserCreate, name: 'userCreate' },
      { path: ':id', component: UserViewPage, name: 'userViewPage' },
      { path: ':id/edit', component: UserEdit, name: 'userEdit' }
    ] },
  { path: '/404', component: ErrorPage, name: '404' },
  { path: '*', redirect: { name: '404' } }
];

export const titles = {
    "users": "Users",
    "404": "Page Not Found",
    "userViewPage": "User Review",
    "userEdit": "User Edit",
    "userCreate": "Create User"
}
