# user-management

> Simple Api for user management

## Requirments
In order to use this project you need to install Node and some globally installed Node packages: "json-server"..

``` bash
# Install Node;
https://nodejs.org/en/

# install json-server globally;
npm install -g json-server
```

## Setting up the project
``` bash
# Install dependencies;
npm install
```

## Starting up

``` bash
# start up the server (localhost:4444)
npm run server

# start up the application (localhost:3333)
npm run dev
```

If all steps done properly the application should open up in default browser;
Or reached by typing this address: localhost:3333

For detailed explanation on how things work and how to set up the project contact the author - giemik8@gmail.com