var gulp = require('gulp');
var sass = require('gulp-sass');
var prefix = require('gulp-autoprefixer');
var plumber = require('gulp-plumber');
var sourcemaps = require('gulp-sourcemaps');

var source = 'src/style/**/*.scss';
var dest = './';

function onError(err) {
    console.log(err);
}

gulp.task('sass', function(){
    return gulp.src(source)
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(sourcemaps.write({includeContent: false}))
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(prefix({map: true}))
    .pipe(sourcemaps.write('.', {debug: true}))
    .pipe(gulp.dest(dest))
    .pipe(plumber({
        errorHandler: onError
    }));
});

gulp.task('default', function () {
    gulp.watch(source, ['sass']);
});
